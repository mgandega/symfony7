<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;

class MultipleDependenciesTest extends TestCase
{
    public function testUne(): string
    {
        $this->assertTrue(true);
        return 'premiere';
    }
    public function testDeux(): string
    {
        $this->assertFalse(false);
        return 'deuxieme';
    }

    /**
     * @depends testUne
     * @depends testDeux
     *  @requires PHP >= 5.3
     */
    public function testUnique($a, $b)
    {
        $this->assertSame('premiere', $a);
        $this->assertSame('deuxieme', $b);
        // $this->markTestIncomplete("ce teste est incomplet");
    }
}
