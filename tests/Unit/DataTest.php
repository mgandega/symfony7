<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;

class DataTest extends TestCase
{
    /**
     * @dataProvider additionWithPositiveDataProvider
     * @dataProvider additionWithNegativeDataProvider
     */
    public function testAddition($a, $b, $expected): void
    {
        // if(gettype($a) == 'string' || gettype($b) == 'string' || gettype($expected) == 'string'){
            $this->assertEquals($expected, $a + $b);
        // }else{

        // }

    }


    public function additionWithPositiveDataProvider()
    {
        return [
            [0, 0, 0],
            [0, 1, 1],
            [1, 0, 1],
        ];
    }
    public function additionWithNegativeDataProvider()
    {
        return [
            [-1, -2, -3],
            [-5, -2, -7],
            [-7, -9, -16]
        ];
    }

  
}
