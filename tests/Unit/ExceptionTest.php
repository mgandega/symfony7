<?php

namespace App\Tests\Unit;

use App\Controller\ProductController;
use PHPUnit\Framework\TestCase;

class ExceptionTest extends TestCase
{
    /**
     * @dataProvider exceptionWithStringDataProvider
     */
    public function testSomething($a, $price): void
    {
        $product = new ProductController;
        if ($a != 'food' and (gettype($a) == 'string' || gettype($price) == 'string')) {
            $this->expectException('Exception');
            $product->index($price, $a);
        }
    }

    public function exceptionWithStringDataProvider()
    {
        return [
            ['hello', 200],
            [150, 'hello'],
            ['hello', 'hello']
        ];
    }
}
