<?php

namespace App\Tests\Functionnal;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ConferencesPageTest extends WebTestCase
{
    public function testPage(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/');

        // $this->assertSelectorTextContains('h5', 'Hello World');
        // $bouton = $crawler->filter('h5.card-title');
        // $this->assertSame(1,count($bouton));
        $this->assertSame(10,10);
        // $this->assertResponseIsSuccessful();
    }
}
