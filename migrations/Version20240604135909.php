<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240604135909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commentaire CHANGE conference_id conference_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE conference CHANGE id id VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE conference_competence ADD CONSTRAINT FK_6A4CDF58604B8382 FOREIGN KEY (conference_id) REFERENCES conference (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE conference_competence ADD CONSTRAINT FK_6A4CDF5815761DAB FOREIGN KEY (competence_id) REFERENCES competence (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reservation CHANGE conference_id conference_id VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commentaire CHANGE conference_id conference_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE conference CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE conference_competence DROP FOREIGN KEY FK_6A4CDF58604B8382');
        $this->addSql('ALTER TABLE conference_competence DROP FOREIGN KEY FK_6A4CDF5815761DAB');
        $this->addSql('ALTER TABLE reservation CHANGE conference_id conference_id INT NOT NULL');
    }
}
