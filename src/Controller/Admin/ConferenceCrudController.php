<?php

namespace App\Controller\Admin;

use DateTime;
use App\Entity\Image;
use App\Entity\Categorie;
use App\Entity\Conference;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ConferenceCrudController extends AbstractCrudController
{
    private SluggerInterface $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public static function getEntityFqcn(): string
    {
        return Conference::class;
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Conférenc::e')
            ->setEntityLabelInPlural('Conférences')
            ->setPageTitle('index', 'liste des conférences')
            ->setPageTitle('edit', 'page d\'édition')
            // ->setPageTitle('detail', 'les détails sur la conférence')
            ->setPageTitle('detail', fn (Conference $conference) => (string) $conference)
            ->setPageTitle('new', 'nouvelle conférence')

            // ...
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('titre'),
            TextField::new('description'),
            // Field::new('image.file', 'Image File')
            //     ->setFormType(FileType::class)
            //     ->setLabel('Upload Image'),
            //     // ->onlyOnForms(),
            TextField::new('lieu'),
            AssociationField::new('categorie'),
            ImageField::new('image.url')
                ->setBasePath('uploads/images')
                ->setUploadDir('public/uploads/images')
                ->setUploadedFileNamePattern('[randomhash].[extension]')
                ->setRequired(false),


        ];
    }

    public function persistEntity(EntityManagerInterface $em, $entityInstance): void
    {
        $this->handleFileUpload($entityInstance);
        
        parent::persistEntity($em, $entityInstance);
    }
    
    public function updateEntity(EntityManagerInterface $em, $entityInstance): void
    {
        $this->handleFileUpload($entityInstance);
        
        parent::updateEntity($em, $entityInstance);
    }
    
    private function handleFileUpload($entityInstance): void
    {
        if (!$entityInstance instanceof Conference) {
            return;
        }
        
        $image = $entityInstance->getImage();
        if ($image === null) {
            $image = new Image();
            $entityInstance->setImage($image);
            $image->setUrl('uploads/img/avatar.png');
            $image->setAlt('uploads/img/avatar.png');
        }
        
        $file = $image->getFile();
        
        
        if ($file instanceof UploadedFile) {
            $newFilename = $this->uploadFile($file);
            $image->setUrl('uploads/images/' . $newFilename);
            $image->setAlt($newFilename);
            $image->setFile($file);
        } elseif ($image->getUrl() === null) {
            $image->setUrl('uploads/img/avatar.png');  // Mettre une image par défaut ou null
            $image->setAlt('uploads/img/avatar.png');  // Mettre une image par défaut ou null
        }
    }
    
    private function uploadFile(UploadedFile $file): string
    {
        
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

        try {
            $file->move(
                $this->getParameter('images_directory'),
                $newFilename
            );
        } catch (FileException $e) {
            // gérer les exceptions si quelque chose se passe mal pendant le déplacement du fichier
        }

        return $newFilename;
    }
}
