<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Conference;
use App\Entity\Commentaire;
use App\Entity\Reservation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    #[Route('/profil/{idUser}/{idConf}', name: 'app_user_conf', defaults: ['idConf' => ''])]
    #[Route('/profil/{idUser}', name: 'app_user')]
    public function index(EntityManagerInterface $manager, $idUser, $idConf): Response
    {
        $commentaires = '';
        $user = $manager->getRepository(User::class)->find($idUser);
        $conferences =  $manager->getRepository(Conference::class)->findBy(['user' => $user]);
        $reservations =  $manager->getRepository(Reservation::class)->findBy(['user' => $user]);
        if (!empty($idConf)) {
            $conference =  $manager->getRepository(Conference::class)->find($idConf);
            $commentaires =  $manager->getRepository(Commentaire::class)->findBy(['conference' => $conference]);
            // dd($commentaires);
        }
        //    $commentaires = $manager->getRepository(Commentaire::class)->findBy([]);

        return $this->render('profil/index.html.twig', [
            'conferences' => $conferences,
            'user' => $user,
            'commentaires' => $commentaires,
            'reservations' => $reservations
        ]);
    }

    // #[route("detailsConferences/{idConf}" , name:"details_conferences")]
    // public function details(EntityManagerInterface $manager,$idConf ){

    //     return $this->render('profil/index.html.twig', [
    //         'conferences' => $conferences,
    //         'user'=>$user
    //     ]);
    // }
}
