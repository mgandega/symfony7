<?php

namespace App\Controller;

use Exception;
use ArrayObject;
use App\Antispam;
use DateTimeImmutable;
use App\Entity\Categorie;
use App\Entity\Competence;
use App\Entity\Conference;
use App\Events\Evenements;
use App\Form\CompetenceType;
use App\Form\ConferenceType;
use App\Listeners\Bienvenue;
use App\Events\ExceptionEvent;
use App\Events\ConferenceEvents;
use App\Events\AjoutConferenceEvent;
use App\Repository\CategorieRepository;
use App\Security\Voter\ConferenceVoter;
use App\Repository\ConferenceRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use PHPUnit\Framework\Constraint\ObjectHasProperty;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ConferencesController extends AbstractController
{

    public $em;
    public function __construct(EntityManagerInterface $manager)
    {
        $this->em = $manager;
    }

    #[Route('/conference/details/{id}', name: 'conference.details', requirements: ['id' => '\d+'])]
    public function details($id)
    {

        $conference = $this->em->getRepository(Conference::class)->find($id);
        // $categories = $this->em->getRepository(Categorie::class)->findAll();
        // $categories = [];

        // if (!in_array($conference->getCategorie()->getNom(), $categories)) {
        //     $categories[]['nom'] = $conference->getCategorie()->getNom();
        // }
        $categories = $this->em->getRepository(Categorie::class)->findAll();

        return $this->render("conferences/conference.html.twig", ['conference' => $conference, 'categories' => $categories]);
    }

    #[Route('/conference/edit/{id}', name: 'conference.edit')]
    #[IsGranted('view', 'conference')]
    public function editer(Request $request, Conference $conference, $id)
    {
        $conference = $this->em->getRepository(Conference::class)->find($id);

        $form = $this->createForm(ConferenceType::class, $conference, ['button_label' => 'Modifier une conférence']);
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isvalid()) {

            $conference = $form->getData();
            $dossier_images = $_SERVER['DOCUMENT_ROOT'] . "/uploads/images";
            if ($conference->getImage()->getFile() instanceof UploadedFile) {
                $filename = rand(1000, 9999) . time() . '_' . $form->getData()->getImage()->getFile()->getClientOriginalName();
                // dd(get_class_methods($form->getData()->getImage()->getFile()));
                $bddFile = "uploads/images";
                $file = $conference->getImage()->getFile();
                $file->move($dossier_images, $filename);
                $conference->getImage()->setUrl($bddFile . '/' . $filename);
                $conference->getImage()->setAlt($conference->getImage()->getFile()->getClientOriginalName());
                $conference->getImage()->setFile($conference->getImage()->getFile());
                $chemin = $bddFile . '/' . $filename;
            }
            // on utilise seulement le flush() si on veut supprimer ou modifier
            // si on utilise persist() et flush(), on rajoutera une autre ligne dans la table
            $this->em->flush();
            return $this->redirectToRoute('conference.index');
        }
        $categories = $this->em->getRepository(Categorie::class)->findAll();
        return $this->render("conferences/edit.html.twig", ['form' => $form->createView(), 'categories' => $categories, 'bouton' => 'modifier']);
    }
    #[Route('/conference/supp/{id}', name: 'conference.supp')]
    public function delete($id)
    {
        $conference = $this->em->getRepository(Conference::class)->find($id);
        // dd($conference);
        $this->em->remove($conference); // pour supprimer
        $this->em->flush();
        return $this->redirectToRoute('conference.index');
    }

    #[Route('/conference/add', name: 'conference.add')]
    public function add(Request $request, ValidatorInterface $validator, Antispam $antispam, EventDispatcherInterface $dispatcher)
    {
        $dossier_images = $_SERVER['DOCUMENT_ROOT'] . '/uploads/images';

        $conference = new Conference();
        $form = $this->createForm(ConferenceType::class, $conference, ['button_label' => 'Ajouter une conférence', 'validation_groups' => ['create']]);
        // $form = $this->createForm(ConferenceType::class, $conference);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                // $dispatcher->dispatch(new AjoutConferenceEvent($conference->getDescription(), $this->getUser()));
                // dd('okay');
                // dd($form->get('description')->getData());
                // if ($antispam->alert($form->get('description')->getData())) {
                //     return new Response('ce message est un spam');
                // }
                $imagefile = $form->getData()->getImage()->getFile();

                $imageName = time() . $form->getData()->getImage()->getFile()->getClientOriginalName();
                $url = 'uploads/images/' . $imageName;
                $imagefile->move($dossier_images, $imageName);
                $fichier_bd = $dossier_images . '/' . $imageName;
                $conference = $form->getData();
                $conference->getImage()->setFile($imagefile);
                $conference->getImage()->setUrl($url);
                $conference->getImage()->setAlt($imageName);
                $user = $this->getUser();
                $conference->setUser($user);

                //////////////////////////////////////////////////////////////////////////////////////////
                $time = time();

                $session = $request->getSession();
                $date = $session->get('date');
                if (isset($date) and $time - $date > 30) {
                    $session->set('date', $time);

                    // on vérifie si le contenu du message contient un mot interdit
                    $description = $conference->getDescription();
                    $tab = explode(' ', $description); // on transforme la description en tableau où chaque mot de la descriptoin sera un élément du tableau ($tab)
                    $motsInterdits = ["abandon", "demotivation"];
                    // on texte si les mots abandon et demotivation se trouve dans le tableau ($tab)
                    foreach ($motsInterdits as $mot) {
                        if (in_array($mot, $tab)) {
                            // throw new InvalidArgumentException("la description contient au moin un mot interdit");
                            $event = new ExceptionEvent("la description contient au moin un mot interdit");
                            $dispatcher->dispatch($event);
                            die('ok ...');
                        }
                    }
                    // fin vérification
                    $this->em->persist($conference);
                    $this->em->flush();
                    $this->addFlash('success', 'Merci de créer une annonce');
                } else {
                    if ($date) {
                        $session->set('date', $time);
                        echo "passage de moin de 30 secondes";
                    } else {
                        $session->set('date', $time);
                        echo "premier passage";
                    }
                }

                ////////////////////////////////////////////////////////////



                $dispatcher->dispatch(new AjoutConferenceEvent($conference->getDescription(), $this->getUser()));
                return $this->redirectToRoute("conference.index");
            }
        }

        return $this->render("conferences/ajout.html.twig", ['form' => $form->createView(), 'bouton' => 'ajouter']);
    }


    // #[Route('/recherche', name: 'recherche', methods: ['POST'])]
    #[Route('/', name: 'conference.index')]
    #[Route('/conferences/categorie/{nom}', name: 'conference.categorie')]
    public function index(Request $request, CategorieRepository $categorieRepo, PaginatorInterface $paginator, $nom = ''): Response
    {
        // phpinfo();
        if (!empty($request->request->get('date'))) {
            // dd($request->request->get('recherche'));
            $recherche = $request->request->get('date');
            $date = new \DateTimeImmutable($recherche);
            $categories = $this->em->getRepository(Categorie::class)->findAll();
            $lastConferences = $this->em->getRepository(Conference::class)->lastConferences();
            $conferences = $this->em->getRepository(Conference::class)->rechercheParDate($date);
            // dd($conferences->getResult());
            $conferences  = $conferences->getResult();
        } elseif (!empty($request->attributes->get('nom'))) {
            // Récupérer les noms de catégories distincts
            $categoryNames = $this->em->getRepository(Categorie::class)->createQueryBuilder('c')
                ->select('DISTINCT c.nom')
                ->getQuery()
                ->getArrayResult();
            $categoryConferences = [];

            // Récupérer les conférences par nom de catégorie

            $confByCategory = $this->em->getRepository(Conference::class)->ConferencesParCategorie($nom);
            $conferences = $confByCategory;
        } else {

            $conferences = $this->em->getRepository(Conference::class)->findAll();
            // dd($categoryConferences);
        }

        $pagination = $paginator->paginate(
            $conferences, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );
        // Récupérer toutes les catégories (pour les liens individuels)
        $categories = $categorieRepo->findAll();
        // die('ok ..');

        // Passer les données au template
        return $this->render('conferences/conferences.html.twig', [
            'conferences' => $pagination,
            'categories' => $categories
        ]);
    }

    #[Route('/conference/categorie/{nom}', name: 'conference.by_category')]
    public function menu($nom): Response
    {
        // Récupérer les conférences par nom de catégorie
        $conferences = $this->em->getRepository(Conference::class)->createQueryBuilder('conf')
            ->innerJoin('conf.categorie', 'cat')
            ->where('cat.nom = :categoryName')
            ->setParameter('categoryName', $nom)
            ->getQuery()
            ->getResult();
        $categories = $this->em->getRepository(Categorie::class)->findAll();
        $lastConferences = $this->em->getRepository(Conference::class)->lastConferences();


        // dd($conferences);
        // Passer les conférences au template
        return $this->render('conferences/menu.html.twig', [
            'conferences' => $conferences,
            'categories' => $categories,
            'lastConferences' => $lastConferences
        ]);
    }
    #[Route('/conferences/recherche', name: 'conference.recherche')]
    public function recherche(Request $request, PaginatorInterface $paginator)
    {

        $session = $request->getSession();

        if ($request->isMethod("POST")) {
            // $date = !empty($request->request->get('date')) ?$request->request->get('date'): (new DateTime())->format("Y-m-d");
            $date = !empty($request->request->get('date')) ? $request->request->get('date') : null;
            $prix = !empty($request->request->get('prix')) ? $request->request->get('prix') : null;
            $categorie = !empty($request->request->get('categorie')) ? $request->request->get('categorie') : null;
            $session->set('dateRecherche', $date);
            $session->set('prix', $prix);
            $session->set('categorie', $categorie);
        } elseif ($request->query->get('page')) {
            $date = $session->get('dateRecherche');
            $prix = $session->get('prix');
            $categorie = $session->get('categorie');
        } elseif ($request->isMethod("GET")) {
            $session->remove('dateRecherche');
            $session->remove('prix');
            $session->remove('categorie');
            $date = null;
            $prix = null;
            $categorie = null;
        }


        $categories = $this->em->getRepository(Categorie::class)->findAll();
        $lastConferences = $this->em->getRepository(Conference::class)->lastConferences();

        // dd($date,$prix,$categorie);
        $conf = $this->em->getRepository(Conference::class)->recherche($date, $prix, $categorie);

        $conferences = $paginator->paginate(
            $conf, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );
        // dd($date);
        // $conferences = $pagination->getItems();
        // $val ='hello';
        return $this->render("conferences/conferences.html.twig", [
            'selectedCategorie' => $categorie,
            'selectedPrix' => $prix,
            'selectedDate' => $date,
            'categories' => $categories,
            'conferences' => $conferences,
            'lastConferences' => $lastConferences
        ]);
        // return $this->render('conferences/conf.html.twig', []);
    }

    #[Route('competence/add', 'conference.ajoutCompetence')]
    public function ajoutCompetence(Request $request)
    {
        // echo $bienvenue->salutation();
        $competence = new Competence();
        $form = $this->createForm(CompetenceType::class, $competence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            // $event =  new ConferenceEvents($competence->getNom(), $this->getUser());

            // $dispatcher = new EventDispatcher();
            // $dispatcher->dispatch($event);
            //    $dispatcher->dispatch($event);
            // $dispatcher->dispatch($event, Evenements::MESSAGE);

            // dd('stop');
            $this->em->persist($competence);
            $this->em->flush();
            return $this->redirectToRoute('conference.index');
        }
        return $this->render("conferences/ajoutCompetence.html.twig", ["form" => $form->createView()]);
    }
}
