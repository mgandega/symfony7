<?php

namespace App\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ProductController extends AbstractController
{
    public const PRODUCT = 'food';
    // public function __construct(public string $name, public string $type, public float $price)
    // {
    // }

    #[Route('/product', name: 'app_product')]
    public function index($type, $price): float|Exception
    {

        if (self::PRODUCT == $type) {
            return $price * 0.5;
        } else {
            throw new \Exception("le produit doit être $type");
        }
        // return $this->render('product/index.html.twig', [
        //     'controller_name' => 'ProductController',
        // ]);
    }
}
