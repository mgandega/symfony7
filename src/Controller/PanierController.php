<?php

namespace App\Controller;

use App\Entity\Conference;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PanierController extends AbstractController
{
    public function __construct(private RequestStack $requestStack)
    {
    }
    #[Route('/panier', name: 'app_panier')]
    public function index(EntityManagerInterface $manager, Request $request): Response
    {
        $session = $this->requestStack->getSession();
        $conferenceId = $request->request->get('conferenceId');
        $quantite = $request->request->get('quantite');
        if ($conferenceId && $quantite) {
            $conference = $manager->getRepository(Conference::class)->find($conferenceId);
            $monPanier =  $this->traitementPanier($_POST['conferenceId'], $_POST['quantite'], $conference->getTitre(), $conference->getDescription(), $conference->getPrix());
        }

        $panier = $this->getPanier();
        $prixTotal = $this->calculerPrixTotal();

        return $this->render('panier/index.html.twig', [
            'panier' => $panier,
            'prixTotal' => $prixTotal
            // 'conference' => $conference
        ]);
    }

    // on utilise cette fonction si on ajoute pour la prémière fois un produitz au panier
    public function initialisationPanier()
    {
        $session = $this->requestStack->getSession();
        // si on arrive pour la premiere fois cette condition retournera true
        if (!$session->has('panier')) {

            // array() => []
            $session->set('panier', [
                'conferenceId' => [],
                'titre' => [],
                'quantite' => [],
                'prix' => [],
            ]);
        }
    }

    public function traitementPanier($conferenceId, $quantite, $titre, $description, $prix)
    {
        $session = $this->requestStack->getSession();
        // initialisation du panier 
        $this->initialisationPanier();
        $panier = $session->get('panier');

        $position = array_search($conferenceId, $panier['conferenceId']);


        // if ($position == true) != $position !== false

        // si le produit existe déja dans le panier
        if ($position !== false) {
            // $panier['quantite'][$position] += $quantite;
            $panier['quantite'][$position] += $quantite;
        } else {
            $panier['conferenceId'][] = $conferenceId;
            $panier['titre'][] = $titre;
            $panier['quantite'][] = $quantite;
            $panier['prix'][] = $prix;
        }

        // mise à jour du panier (en session)
        $session->set('panier', $panier);
    }

    public function getPanier()
    {
        $session = $this->requestStack->getSession();
        $panier = $session->get('panier');
        return  $panier;
    }

    // function retirerProduitDuPanier($id_produit_a_supprimer)
    // {
    //     $position_produit = array_search($id_produit_a_supprimer, $_SESSION['panier']['id_produit']);
    //     if ($position_produit !== false) {
    //         array_splice($_SESSION['panier']['titre'], $position_produit, 1);
    //         array_splice($_SESSION['panier']['id_produit'], $position_produit, 1);
    //         array_splice($_SESSION['panier']['quantite'], $position_produit, 1);
    //         array_splice($_SESSION['panier']['prix'], $position_produit, 1);
    //     }
    // }

    #[Route('panier_supprimer/{conferenceId}', name: 'app_panier_supprimer')]
    public function app_panier_supprimer($conferenceId)
    {
        $this->supprimerProduit($conferenceId);

        return $this->redirectToRoute('app_panier');
    }

    #[Route('supprimerProduit', name: 'supprimerProduit')]
    public function supprimerProduit($conferenceId)
    {
        $panier = $this->getPanier();

        $position = array_search($conferenceId, $panier['conferenceId']);
        if ($position !== false) {
            $session = $this->requestStack->getSession();
            // Suppression des données correspondantes à l'index trouvé
            array_splice($panier['conferenceId'], $position, 1);
            array_splice($panier['titre'], $position, 1);
            array_splice($panier['quantite'], $position, 1);
            array_splice($panier['prix'], $position, 1);

            $session->set('panier', $panier);
        }
    }
    public function calculerPrixTotal()
    {
        $panier = $this->getPanier();
        $total = 0;

        foreach ($panier['quantite'] as $index => $quantite) {
            $total += $quantite * $panier['prix'][$index];
        }

        return $total;
    }
}
