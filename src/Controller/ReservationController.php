<?php

namespace App\Controller;

use App\Entity\Conference;
use App\Entity\Reservation;
use App\Form\ReservationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ReservationController extends AbstractController
{
    #[Route('/reservation/{id}', name: 'app_reservation')]
    public function index(EntityManagerInterface $manager, Request $request, $id): Response
    {
        $reservation =  new Reservation();
        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);
        $conference = $manager->getRepository(Conference::class)->find($id);
        if ($form->isSubmitted() && $form->isValid()) {
            $reservation = $form->getData();
            $reservation->setConference($conference);
            $reservation->setUser($this->getUser());
            $reservations = $manager->getRepository(Reservation::class)->findBy(['user' => $this->getUser(), 'conference' => $conference]);

            if (count($reservations) > 0) {
                $this->addFlash('failure', "vous avez déja reservé pour cette annonce");
                return $this->redirectToRoute('app_reservation', ['id' => $id]);
            }

            $manager->persist($conference);
            $manager->persist($reservation);
            $manager->flush();
            return $this->redirectToRoute('conference.index');
        }
        return $this->render('reservation/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    // #[Route('/edit/{id}', name: 'edit_reservation')]
    // public function edit(Request $request, $id)
    // {
    //     $reservation =   new Reservation();
    //     $form = $this->createForm(ReservationType::class, $reservation);
    //         $form->handleRequest($request);

    // }
}
