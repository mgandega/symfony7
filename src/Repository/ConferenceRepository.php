<?php

namespace App\Repository;

use App\Entity\Conference;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Conference>
 *
 * @method Conference|null find($id, $lockMode = null, $lockVersion = null)
 * @method Conference|null findOneBy(array $criteria, array $orderBy = null)
 * @method Conference[]    findAll()
 * @method Conference[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConferenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Conference::class);
    }

    //    /**
    //     * @return Conference[] Returns an array of Conference objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    //    /**
    //     * @return Conference[] Returns an array of Conference objects
    //     */
    //    public function findByName(): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    /**
     * @return Conference[] Returns an array of Conferences objects
     */
    public function ConferencesParCategorie($nom): array
    {
        return $this->createQueryBuilder('conf')
            ->innerJoin('conf.categorie', 'ca')
            ->where('ca.nom = :nom')
            ->setParameter('nom', $nom)
            ->getQuery()
            ->getResult();
    }
    /**
     * @return Conference[] Returns an array of Conferences objects
     */
    public function recherche($date,$prix,$categorie)
    {       dd($date,$prix,$categorie);
            $qb =  $this->createQueryBuilder('c')
            ->innerJoin('c.categorie','categorie');
            if($date != null){
               $qb->andWhere('c.date <= :date')
               ->setParameter('date', $date);
            }
            if($prix != null){
                $qb->andWhere('c.prix <= :prix')
                ->setParameter('prix', $prix);
            }
            if($categorie != null){
                $qb->andWhere('categorie.nom = :nom')
                ->setParameter('nom', $categorie);
            }

            return $qb->getQuery() ;
            // ->getResult();   
            
    }
    /**
     * @return Conference[] Returns an array of Conferences objects
     */
    public function lastConferences(): array
    {
        return $this->createQueryBuilder('conf')
            ->setMaxResults(5)
            ->orderBy('conf.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    //    public function findOneBySomeField($value): ?Conference
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
