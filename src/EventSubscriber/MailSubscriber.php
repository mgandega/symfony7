<?php

namespace App\EventSubscriber;

use Twig\Environment;
use App\Entity\Conference;
use Symfony\Component\Mime\Email;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MailSubscriber implements EventSubscriberInterface
{
    public function __construct(
        public MailerInterface $mailer,
        public Environment $twig
        // public ContainerInterface $container
    ) {
    }

    public function onAjoutConferenceEvent($event): void
    {

        $message = 'Sending emails is fun again!';
        $name = 'John Doe';

        // Génération du contenu HTML avec Twig
        $htmlContent = $this->twig->render('contact\mail.html.twig', [
            'message' => $message,
            'name' => $name
        ]);

        $email = (new Email())
            ->from('hello@example.com')
            ->to('you@example.com')
            ->subject('Time for Symfony Mailer!')
            ->text($message)
            ->html($htmlContent);

        $this->mailer->send($email);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'App\Events\AjoutConferenceEvent' => 'onAjoutConferenceEvent',
        ];
    }
}
