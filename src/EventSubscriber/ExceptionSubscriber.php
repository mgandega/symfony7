<?php

namespace App\EventSubscriber;


use Throwable;
use Twig\Environment;
use App\Events\ExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Loader\Configurator\twig;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(public Environment $twig)
    {
    }
    public function onKernelException(ExceptionEvent $event)
    {
        $response = new Response();
        dd('<div>hello</div>' . $response->getContent());

        dd('ExceptionEvent listener');
        return 'ok';
    }

    public static function getSubscribedEvents(): array
    {
        return [
            // KernelEvents::EXCEPTION => 'onKernelException',
            ExceptionEvent::class => 'onKernelException',
        ];
    }
}
