<?php
// src/Security/PostVoter.php
namespace App\Security\Voter;

use App\Entity\Conference;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ConferenceVoter extends Voter
{
    // these strings are just invented: you can use anything
    const VIEW = 'view';
    const EDIT = 'edit';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // if the attribute isn't one we support, return false

        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }
        // if (in_array($attribute, [self::VIEW, self::EDIT]) && $subject instanceof Conference) {
        //     return true;
        // }
        
        // return in_array($attribute, [self::VIEW, self::EDIT]) && $subject instanceof Conference ;
        
        // only vote on `Post` objects
        // if (!$subject instanceof Conference) {
        //     return false;
        // }
        // dd(in_array($attribute, [self::VIEW, self::EDIT]) && $subject instanceof Conference);

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
     
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }
        // you know $subject is a Post object, thanks to `supports()`
        /** @var Conference $conference */  
        $conference = $subject;
        

        return match($attribute) {
            self::VIEW => $this->canView($conference, $user),
            self::EDIT => $this->canEdit($conference, $user),
            default => throw new \LogicException('This code should not be reached!')
        };
    }

    private function canView(Conference $conference, User $user): bool
    {
        // if they can edit, they can view

        // if ($this->canEdit($conference, $user)) {
        //     return true;
        // }
        // // the Post object could have, for example, a method `isPrivate()`
        // // return !$conference->isPrivate();
        // return true;
        // dd($user , $conference->getUser());
        return $user === $conference->getUser();
    }

    private function canEdit(Conference $conference, User $user): bool
    {
        // this assumes that the Post object has a `getOwner()` method
        return $user->getId() === $conference->getUser()->getId();
    }
}