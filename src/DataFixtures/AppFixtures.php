<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Image;
use App\Entity\Categorie;
use App\Entity\Competence;
use App\Entity\Conference;
use App\Entity\Commentaire;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public $lieu = ['lyon', 'paris', 'marseille', 'monaco', 'lens'];
    public $role = [['ROLE_USER'],['ROLE_ADMIN']];

    public function __construct(public UserPasswordHasherInterface $passwordHasher){ }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $faker->addProvider(new \Xvladqt\Faker\LoremFlickrProvider($faker));
        $cat = ['conference', 'seminaire', 'meeting'];
        $categories = [];

        for ($i = 0; $i < 3; $i++) {
            $categorie = new Categorie();
            $categorie->setNom($cat[$i]);
            $manager->persist($categorie);
            $this->addReference('categorie' . $i, $categorie);
            $categories[] = $categorie;
        }

        $com = ['html/css', 'php', 'symfony', 'laravel', 'python'];
        $competences = [];
        $users =[];
        for ($i = 0; $i < 10; $i++){
           $user =  new User();
           $user->setEmail($faker->email())
           ->setFirstName($faker->name)
           ->setLastName($faker->name);
          $hashedPassword =  $this->passwordHasher->hashPassword(
            $user,
            $faker->name
        );
        $user->setPassword($hashedPassword);

           $user->setRoles($faker->randomElement([['ROLE_USER'],['ROLE_ADMIN']]));
           $manager->persist($user);
           $this->addReference('user'.$i, $user);
            $users[] = $user;
             
        }

        for ($i = 0; $i < 5; $i++) {
            $comptence = new Competence();
            $comptence->setNom($com[$i]);
            $manager->persist($comptence);
            $this->addReference('competence' . $i, $comptence);
            $competences[] = $comptence;
        }

        for ($i = 0; $i < 10; $i++) {
            $image = new Image();
            $fichier = $faker->image(null, 360, 360, 'animals', true, true, 'cats', true, 'jpg');
            $info = pathinfo($fichier);
            $fileName = $info['basename'];
            $destinationPath = 'public/uploads/images/' . $fileName;
            copy($fichier, $destinationPath);

            // Créer un UploadedFile pour l'image
            $uploadedFile = new UploadedFile(
                $destinationPath,
                $fileName
            );
            // Hydrater les attributs de l'image
            $image->setUrl('uploads/images/' . $fileName);
            $image->setAlt($faker->sentence());
            $image->setFile($uploadedFile);

            // Persist et flush l'entité Image dans la base de données
            $manager->persist($image);
            $this->addReference('image' . $i, $image);
        }
        for ($i = 0; $i < 10; $i++) {
            $conference = new Conference();
            $conference->setTitre($faker->name)
                ->setDescription($faker->sentence())
                ->setLieu($faker->randomElement($this->lieu))
                ->setUser($this->getReference('user'.$i))
                ->setPrix(round(rand(50, 300)));
            // ->setLieu($faker->randomElement($this->lieu[$faker->numberBetween(0, 4)]));

            if ($i < 3) {

                $conference->setCategorie($categories[$i]);
            } else {
                $conference->setCategorie($faker->randomElement($categories));
            }
            if ($i < 5) {

                $conference->addCompetence($competences[$i]);
            } else {
                $conference->addCompetence($faker->randomElement($competences));
            }



            // ->setImage($image);
            $conference->setImage($this->getReference('image' . $i));

            $manager->persist($conference);
            $this->addReference('conference' . $i, $conference);
        }
        for ($i = 0; $i < 10; $i++) {
            $com = new Commentaire();
            $com->setPseudo($faker->name());
            $com->setContent($faker->name());
            $com->setConference($this->getReference('conference' . $i));
            $manager->persist($com);
        }
        $manager->flush();
    }
}
