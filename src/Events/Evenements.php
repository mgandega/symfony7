<?php

namespace App\Events;

use Symfony\Contracts\EventDispatcher\Event;

final class Evenements extends Event
{
    public const MESSAGE = "conference.conferenceEvents";
}
