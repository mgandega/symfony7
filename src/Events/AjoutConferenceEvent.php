<?php

namespace App\Events;

use Symfony\Component\Mailer\MailerInterface;

class AjoutConferenceEvent
{

    public function __construct(public $message, public $user)
    {
    }

    public function getMessage()
    {
        return "juste un petit message";
    }
}
