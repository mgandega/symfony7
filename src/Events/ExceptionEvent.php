<?php

namespace App\Events;

class ExceptionEvent
{
    public function __construct(public $message)
    {
    }
}
