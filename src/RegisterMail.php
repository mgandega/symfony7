<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use Twig\Environment;

class RegisterMail extends AbstractController
{
    public function __construct(
        public MailerInterface $mailer,
        public Environment $twig,
        // public ContainerInterface $container
    ) {
    }

    public function send($firstname, $lastname, $email)
    {

        // Génération du contenu HTML avec Twig
        $htmlContent = $this->twig->render('contact\registeredMail.html.twig', [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email
        ]);

        $email = (new Email())
            ->from($email)
            ->to('you@example.com')
            ->subject('Time for Symfony Mailer!')
            //     ->text($message)
            ->html($htmlContent);

        $this->mailer->send($email);
    }
}
