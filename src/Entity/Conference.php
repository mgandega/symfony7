<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ConferenceRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

#[ORM\Entity(repositoryClass: ConferenceRepository::class)]
#[HasLifecycleCallbacks]
class Conference
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private  $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: "Le titre ne doit pas être vide")]
    #[Assert\Length(
        min: 2,
        max: 50,
        minMessage: 'Votre titre doit avoir au moins {{ limit }} caractères ...',
        maxMessage: 'Votre titre ne peut pas dépasser {{ limit }} caractères'
    )]
    private ?string $titre = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: "La description ne doit pas être vide", groups: ['create'])]
    #[Assert\Length(
        min: 2,
        max: 50,
        minMessage: 'Votre description doit avoir au moins {{ limit }} caractères',
        maxMessage: 'Votre description ne peut pas dépasser {{ limit }} caractères'
    )]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    private ?string $lieu = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $date = null;

    #[Assert\NotBlank(["message" => "ne doit pas être vide"])]
    #[ORM\ManyToOne(inversedBy: 'conferences')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Categorie $categorie = null;

    #[Assert\Valid]
    #[ORM\OneToOne(inversedBy: 'conference', cascade: ['persist', 'remove'])]
    private ?Image $image = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 0, nullable: true)]
    private ?string $prix = null;

    /**
     * @var Collection<int, Commentaire>
     */
    #[ORM\OneToMany(targetEntity: Commentaire::class, mappedBy: 'conference', cascade: ['persist', 'remove'])]
    private Collection $commentaires;

    #[ORM\Column(nullable: true)]
    private ?int $favorite = null;

    #[ORM\ManyToOne(inversedBy: 'conferences')]
    private ?User $user = null;

    /**
     * @var Collection<int, Reservation>
     */
    #[ORM\OneToMany(targetEntity: Reservation::class, mappedBy: 'conference')]
    private Collection $reservations;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $updateddAt = null;

    #[ORM\Column(nullable: true)]
    private  $nbReservations = 0;

    /**
     * @var Collection<int, Competence>
     */
    #[ORM\ManyToMany(targetEntity: Competence::class, inversedBy: 'conferences')]
    private Collection $competence;

    public function __construct()
    {
        if ($this->image === null) {
            $this->image = new Image(); // Initialisez toujours un nouvel objet Image si c'est null
        }
        $this->date = new DateTimeImmutable();
        $this->commentaires = new ArrayCollection();
        $this->reservations = new ArrayCollection();
        $this->competence = new ArrayCollection();
    }
    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(string $lieu): static
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): static
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): static
    {
        $this->image = $image;

        return $this;
    }

    public function nbConferences()
    {
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(?string $prix): static
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * @return Collection<int, Commentaire>
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): static
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires->add($commentaire);
            $commentaire->setConference($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): static
    {
        if ($this->commentaires->removeElement($commentaire)) {
            // set the owning side to null (unless already changed)
            if ($commentaire->getConference() === $this) {
                $commentaire->setConference(null);
            }
        }

        return $this;
    }

    public function getFavorite(): ?int
    {
        return $this->favorite;
    }

    public function setFavorite(?int $favorite): static
    {
        $this->favorite = $favorite;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): static
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations->add($reservation);
            $reservation->setConference($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): static
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getConference() === $this) {
                $reservation->setConference(null);
            }
        }

        return $this;
    }

    #[ORM\PreUpdate]
    public function setUpdateddAt(): static
    {
        $this->updateddAt = new DateTimeImmutable();

        return $this;
    }

    public function getUpdateddAt(): ?\DateTimeImmutable
    {
        return $this->updateddAt;
    }

    public function setIncremente(): static
    {
        $this->nbReservations++;

        return $this;
    }

    public function setDecremente(): static
    {
        $this->nbReservations--;

        return $this;
    }

    public function getNbReservations(): ?string
    {
        return $this->nbReservations;
    }

    public function setNbReservations(?string $nbReservations): static
    {
        $this->nbReservations = $nbReservations;

        return $this;
    }

    /**
     * @return Collection<int, Competence>
     */
    public function getCompetence(): Collection
    {
        return $this->competence;
    }

    public function addCompetence(Competence $competence): static
    {
        if (!$this->competence->contains($competence)) {
            $this->competence->add($competence);
        }

        return $this;
    }

    public function removeCompetence(Competence $competence): static
    {
        $this->competence->removeElement($competence);

        return $this;
    }
}
