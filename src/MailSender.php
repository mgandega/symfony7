<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use Twig\Environment;

class MailSender extends AbstractController
{
    public function __construct(
        public MailerInterface $mailer,
        public Environment $twig,
        // public ContainerInterface $container
    ) {
    }

    public function send()
    {
        $message = 'Sending emails is fun again!';
        $name = 'John Doe';

        // Génération du contenu HTML avec Twig
        $htmlContent = $this->twig->render('contact\mail.html.twig', [
            'message' => $message,
            'name' => $name,
            'language' => $this->getParameter('language')
        ]);

        $email = (new Email())
            ->from('hello@example.com')
            ->to('you@example.com')
            ->subject('Time for Symfony Mailer!')
            ->text($message)
            ->html($htmlContent);

        $this->mailer->send($email);
    }
}
